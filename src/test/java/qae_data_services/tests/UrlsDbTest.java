package qae_data_services.tests;

import com.frameworkium.core.api.tests.BaseTest;
import org.testng.annotations.Test;
import qae_data_services.service.UrlsDbService;

import static com.google.common.truth.Truth.assertThat;

public class UrlsDbTest extends BaseTest {

    @Test(description = "Get all urls")
    public final void planJourneyTest() {

        UrlsDbService service = new UrlsDbService();
        assertThat(service.getURLs().size()).isAtLeast(10000);
    }
}
