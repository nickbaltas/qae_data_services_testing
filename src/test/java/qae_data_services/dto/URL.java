package qae_data_services.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class URL {
    public Object _id;
    public String url;
}
