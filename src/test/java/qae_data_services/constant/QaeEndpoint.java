package qae_data_services.constant;

import com.frameworkium.core.api.Endpoint;

public enum QaeEndpoint implements Endpoint {

    BASE_URI("http://localhost:8087"),
    URLS_DB("/urls_db/get_urls"),
    DATA_GENERATOR("/data_generator"),
    EMAIL_SENDER("/email_sender"),
    LOGGER("/logger"),
    TAP_ORCHESTRATION("/tap_orchestration");

    private String url;

    QaeEndpoint(String url) {
        this.url = url;
    }

    /**
     * @param params Arguments referenced by the format specifiers in the url.
     * @return A formatted String representing the URL of the given constant.
     */
    @Override
    public String getUrl(Object... params) {
        return String.format(url, params);
    }

}
