package qae_data_services.service;

import com.frameworkium.core.api.services.BaseService;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import qae_data_services.constant.QaeEndpoint;

/** Base Service for TFL specific services. */
public class BaseQaeService extends BaseService {

    /**
     * @return a Rest Assured {@link RequestSpecification} with the baseUri
     * (and anything else required by most TFL services).
     */
    @Override
    protected RequestSpecification getRequestSpec() {
        return RestAssured.given().baseUri(QaeEndpoint.BASE_URI.getUrl());
    }

    /**
     * @return a Rest Assured {@link ResponseSpecification} with basic checks
     * (and anything else required by most TFL services).
     */
    @Override
    protected ResponseSpecification getResponseSpec() {
        return RestAssured.expect().response();
    }

}
