package qae_data_services.service;

import qae_data_services.dto.URL;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.Arrays;
import java.util.List;

import static qae_data_services.constant.QaeEndpoint.URLS_DB;

public class UrlsDbService extends BaseQaeService {

    @Step("Get URLs")
    public List<URL> getURLs() {
        URL[] urls = request(URLS_DB.getUrl()).as(URL[].class);
        return Arrays.asList(urls);
    }

}
